from typing import List

from pydantic import BaseModel


class AllRecipesOut(BaseModel):
    """Model for response from endpoint '/recipes'"""

    name: str
    views: int
    cooking_time_in_min: int


class RecipeOut(BaseModel):
    """Model for response from endpoint '/recipe/{recipy_idx}'"""

    name: str
    cooking_time_in_min: int
    description: str
    ingredients: List[str]


class NoData(BaseModel):
    """Model for response from endpoints if there is no requested data in db"""

    error_massage: str
