from __future__ import annotations

from typing import Optional, Sequence, TypeVar

from sqlalchemy import Column, ForeignKey, Integer, Row, Table, Text, select
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from sqlalchemy.orm import Mapped, declarative_base, relationship

TRecipe = TypeVar("TRecipe", bound="Recipe")
DATABASE_URL = "sqlite+aiosqlite:///./cookbook.db"
async_engine = create_async_engine(DATABASE_URL, echo=True)
AioSession = async_sessionmaker(async_engine, expire_on_commit=False)
session = AioSession()
Base = declarative_base()

integration_table = Table(
    "recipe_compositions",
    Base.metadata,
    Column("ingredient_id", ForeignKey("ingredients.id"), primary_key=True),
    Column("recipe_id", ForeignKey("recipes.id"), primary_key=True),
)


class Recipe(Base):
    __tablename__ = "recipes"

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    cooking_time_in_min = Column(Integer, nullable=False)
    description = Column(Text, nullable=False)
    views = Column(Integer, default=0)
    ingredients: Mapped[list["Ingredient"]] = relationship(
        "Ingredient",
        secondary=integration_table,
        back_populates="recipes",
        lazy="joined",
    )

    @classmethod
    async def get_all_recipes(
        cls,
    ) -> Sequence[Row[tuple[str | None, int | None, int | None]]]:
        """
        Get all recipes with partial data from table 'recipes' and sort them by
        views(descending) and cooking_time_in_min (ascending).

        :return: recipe: name, views, cooking_time_in_min
        :rtype: Sequence[Row[tuple[str, int, int]]]
        """

        async with session.begin():
            recipes = await session.execute(
                select(cls.name, cls.views, cls.cooking_time_in_min).order_by(
                    cls.views.desc(), cls.cooking_time_in_min
                )
            )
            recipes_details = recipes.all()
        return recipes_details

    @classmethod
    async def get_recipy_details(cls, recipe_id: int) -> Optional[Recipe]:
        """
        Get recipy detail from table 'recipes' and increase number of views + 1
        if recipy id 'recipe_id' is existed.

        :param recipe_id: recipy id
        :type recipe_id: int

        :return: Recipe | None
        :rtype: Optional[TRecipe]
        """

        async with session.begin():
            result = await session.execute(
                select(cls).filter(cls.id == recipe_id)
            )
            recipe = result.unique().scalar()
            if recipe:
                if recipe.views:
                    recipe.views += 1
                else:
                    recipe.views = 0
                return recipe
        return None


class Ingredient(Base):
    __tablename__ = "ingredients"

    id = Column(Integer, primary_key=True)
    name = Column(Text, nullable=False)
    recipes: Mapped[list["Recipe"]] = relationship(
        "Recipe",
        secondary=integration_table,
        back_populates="ingredients",
        lazy="joined",
    )


async def create_tables() -> None:
    """Create tables in db"""

    async with async_engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


async def insert_data() -> None:
    """Insert data in tables 'recipies' and 'ingredients'"""

    async with session.begin():
        recipes = [
            Recipe(
                name="«Салат Цезарь»",
                cooking_time_in_min=60,
                description="Традиционный с курицей",
            ),
            Recipe(
                name="«Салат Весенний»",
                cooking_time_in_min=10,
                description="Сезонный",
            ),
            Recipe(
                name="«Салат Винегрет»",
                cooking_time_in_min=150,
                description="Народный рецепт",
            ),
        ]
        recipes[0].ingredients.extend(
            [
                Ingredient(name="Помидоры"),
                Ingredient(name="Зеленый салат"),
                Ingredient(name="Куриное филе"),
                Ingredient(name="Белый хлеб"),
                Ingredient(name="Соус «Цезарь»"),
                Ingredient(name="Сыр пармезан"),
            ]
        )
        recipes[1].ingredients.extend(
            [
                Ingredient(name="Капуста"),
                Ingredient(name="Зеленый лук"),
                Ingredient(name="Укроп"),
                Ingredient(name="Редиска"),
                Ingredient(name="Огурец"),
                Ingredient(name="Масло"),
            ]
        )
        recipes[2].ingredients.extend(
            [
                Ingredient(name="Солёные огурцы"),
                Ingredient(name="Свекла"),
                Ingredient(name="Картошка"),
                Ingredient(name="Морковка"),
                Ingredient(name="Горошек"),
            ]
        )
        session.add_all(recipes)


async def init_db() -> None:
    """
    Create tables and insert data in tables 'recipies' and 'ingredients' if
    they are not exist
    """

    await create_tables()
    async with session.begin():
        table_exist = await session.execute(select(Recipe.id))
    if not table_exist.scalar():
        await insert_data()


async def close_db_connection() -> None:
    """Close session and dispose connection with db"""

    await session.close()
    await async_engine.dispose()
