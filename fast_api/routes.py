from contextlib import asynccontextmanager
from typing import Union

from fastapi import FastAPI, Response

from fast_api.database import close_db_connection, init_db
from fast_api.models import handle_all_recipies, handle_recipe_details
from fast_api.schemas import AllRecipesOut, NoData, RecipeOut


@asynccontextmanager
async def lifespan(app: FastAPI):
    """
    Initialize db before starting application 'app' and close db connection
    after stopping it

    :param app: FastApi application
    :type app:FastAPI
    """

    await init_db()
    yield
    await close_db_connection()


app = FastAPI(title="CookBook", lifespan=lifespan)


@app.get(
    path="/recipes",
    summary="All recipes",
    description="Cookbook recipes preview details",
    responses={
        200: {"description": "OK", "model": list[AllRecipesOut]},
        400: {
            "description": "There is no recipies in cookbook",
            "model": NoData,
        },
    },
)
async def all_recipes(
    response: Response,
) -> Union[NoData, list[AllRecipesOut]]:
    """Get all recipies from db"""

    result = await handle_all_recipies()
    if result:
        response.status_code = 200
        return [AllRecipesOut(**i_recipy) for i_recipy in result]
    else:
        response.status_code = 400
        return NoData(error_massage="There is no recipies in cookbook")


@app.get(
    path="/recipe/{recipy_idx}",
    summary="Recipy details",
    description="Cookbook recipy full details",
    responses={
        200: {"description": "OK", "model": RecipeOut},
        400: {
            "description": "There is no recipy with provided id",
            "model": NoData,
        },
    },
)
async def recipe_details(
    recipy_idx: int, response: Response
) -> Union[NoData, RecipeOut]:
    """Get recipy from db by id"""

    recipy = await handle_recipe_details(recipy_idx)
    if recipy:
        response.status_code = 200
        return RecipeOut(**recipy)
    else:
        response.status_code = 400
        return NoData(error_massage="There is no recipy with provided id")
